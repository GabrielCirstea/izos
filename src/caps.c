/*
 * Functii pentru capabilitati*/
#include "common.h"
#include <sys/prctl.h>
#include <sys/capability.h>
#define BUF_LEN 0x10000

/*Afiseaza capabilitatiile din /proc */
void print_proc_caps()
{
	FILE *f = fopen("/proc/self/status", "r");
	char buf[BUF_LEN];

	while (fgets(buf, BUF_LEN, f) != NULL)
		if (!strncmp(buf, "Cap", 3))
			printf("%s", buf);
	fclose(f);
}

/*
 * Seteaza capabilitatiile cerute*/
void set_flags(cap_t caps, cap_value_t *cap_list, int n)
{
	cap_set_flag(caps, CAP_PERMITTED, n, cap_list, CAP_SET);
	cap_set_flag(caps, CAP_INHERITABLE, n, cap_list, CAP_SET);
	cap_set_flag(caps, CAP_EFFECTIVE, n, cap_list, CAP_SET);
	printf("Flags set: %s\n", cap_to_text(caps, NULL));
}

/*
 * Seteaza capabilitatiile ambient*/
void set_ambient_caps(cap_value_t *cap_list, int  n)
{
	int i;
	for(i = 0; i < n; i++)
	{
		if(prctl(PR_CAP_AMBIENT, PR_CAP_AMBIENT_RAISE, cap_list[i], 0, 0))
		{
			perror("prctl");
			exit(1);
		}
	}
}

int caps_user(int uid, int gid)
{
	const gid_t groups[] = {gid};
	if(cap_setgroups(groups[0], 1, groups) == -1)
		perror("cap_setgroups");
	if(cap_setuid(uid) == -1)
		perror("cap_setuid");
	if(setuid(uid) == -1)
		perror("setuid");
	return 0;
}

int caps_clear()
{
	cap_t cap = cap_get_proc();
	cap_clear(cap);
	if(cap_set_proc(cap) == -1){
		errExit("cap_set clear");
	}

	cap_free(cap);
	return 0;
}

/* O simpla verificare pentru setul de capabilitati
 * Momentan este un set prefixat */
int caps_check()
{
	cap_value_t wanted_caps[] = {CAP_DAC_OVERRIDE, CAP_SYS_ADMIN, CAP_SETUID,
									CAP_SETGID, CAP_SETPCAP, CAP_SYS_PTRACE};
	int wanted_n = 6;
	cap_t caps = cap_get_proc();

	int i;
	for(i=0; i<wanted_n; i++){
		cap_flag_value_t status = 0;
		if(cap_get_flag(caps, wanted_caps[i], CAP_EFFECTIVE, &status)){
			perror("cap_get_flag");
			return -1;
		}
		if(status != CAP_SET){
			cap_t wanted = cap_init();
			cap_set_flag(wanted, CAP_EFFECTIVE, wanted_n, wanted_caps, CAP_SET);
			cap_set_flag(wanted, CAP_PERMITTED, wanted_n, wanted_caps, CAP_SET);
			ssize_t len;
			char *cap_str = cap_to_text(wanted, &len);
			printf("nu avem cap nr %d\n", i);
			fprintf(stdout, "Capabilitatiile necesare: %s\n", cap_str);
			cap_free(wanted);
			cap_free(cap_str);
			return 0;
		}
	}
	return 1;

	cap_free(wanted_caps);
}
