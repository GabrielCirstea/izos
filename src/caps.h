#ifndef _IZO_CAPS_H
#define _IZO_CAPS_H

void print_proc_caps();
void set_flags(cap_t caps, cap_value_t *cap_list, int n);
void set_ambient_caps(cap_value_t *cap_list, int  n);
int caps_user(int uid, int gid);
int caps_clear();
int caps_check();
#endif	// ifndef
