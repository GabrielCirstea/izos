/* izos_exec.c
 * Scopul este de a spawna noi procese intr-un "izo" */
#include "common.h"
#include "join_ns.h"
#include "cgroups.h"
#include "caps.h"

void print_usage(const char *prog, int do_exit)
{
	fprintf(stderr, "Usage: %s <izo_name> <cmd> [args ...]\n", prog);
	if(do_exit)
		exit(1);
}

long read_pid(const char *path)
{
	int fd = open(path, O_RDONLY);
	if(fd < 0)
		errExit("open");
	char buff[STR_SIZE];
	if((read(fd, buff, STR_SIZE - 1)) < 0)
		errExit("read");
	errno = 0;
	char *ptr;
	long pid = strtol(buff, &ptr, 10);
	if(errno)
		errExit("valoare invalida");
	return pid;
}

void exec_proc(char *argv[])
{
	execvp(argv[0], argv);
	errExit("execvp");
}

void child_func(int pid_target, int pipes[2], int pipes2[2], char **argv)
{
	close(pipes2[0]);
	join_ns(pid_target, t_uid, t_gid);
	close(pipes2[1]);

	close(pipes[1]);
	wait_on_pipe(pipes);
	close(pipes[0]);
	if(!fork())
		exec_proc(argv);
	wait(NULL);

	exit(0);
}

int main(int argc, char **argv)
{
	if(argc < 3)
		print_usage(argv[0], 1);
	if(!caps_check()){
		fprintf(stderr, "Lipsesc capabilitati necesare\n"
				"Foloseste \"sudo set_caps\" sau sudo %s ...\n", argv[0]);
		exit(1);
	}
	char *name = argv[1];
	char path[PATH_MAX];
	sprintf(path, "%s/%s/init.pid", ROOT_PATH, name);
	long pid = read_pid(path);

	struct cg_limits *cg = cg_init(NULL, NULL);
	cg->read(pid, cg);


	int pipes[2], pipes2[2];
	if(pipe(pipes) == -1)
		errExit("pipe");
	if(pipe(pipes2) == -1)
		errExit("pipe2");

	pid_t child = fork();
	if(!child){
		child_func(pid, pipes, pipes2, argv + 2);
	}
	close(pipes2[1]);
	wait_on_pipe(pipes2);
	close(pipes2[0]);

	close(pipes[0]);
	cg->add_proc(cg, child);
	close(pipes[1]);

	cg_add_raw_proc(CGROUP_PATH, child);

	wait(NULL);
	cg_free(&cg);
}
