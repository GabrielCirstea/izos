/* cgroup2.c
 * Aici sunt functiile pentru manipularea fisierelor din cgroup v2*/
#include "cgroups.h"

#define TIMEOUT_DEFAULT 100000

static const char CG_FILES[][32] = {
	"cpu.max",
	"cpu.weight",
	"memory.high",
	"memory.max",
	"memory.swap.high",
	"memory.swap.max",
};
static const int CG_FILES_N = 6;

enum CG_FILES_NO {CPU_QUOTA = 0, CPU_TIMEOUT = 0,
   CPU_WEIGHT, 	MEMORY_HIGH, MEMORY_MAX, MEMORY_SWAP_HIGH, MEMORY_SWAP_MAX};

static int get_file_no(const char *file_str)
{
	int i;
	while(CG_FILES[i]){
		if(strstr(file_str, CG_FILES[i]))
			return i;
		i++;
	}
	 return -1;
}

int set_if_ramlimit(char *path, long unsigned mem_high, long unsigned mem_max)
{
	int init_length = strlen(path);
	char limit[STR_SIZE];
	if(mem_high)
	{
		strncat(path,"/memory.high", STR_SIZE - 1);
		sprintf(limit, "%u", mem_high);
		if(mem_high == -1)
			strcpy(limit, "max");
		write_in(path, limit);
		path[init_length] = '\0';
	}
	if(mem_max)
	{
		strncat(path,"/memory.max", STR_SIZE - 1);
		sprintf(limit, "%u", mem_max);
		if(mem_max == -1)
			strcpy(limit, "max");
		write_in(path, limit);
		path[init_length] = '\0';
	}
	return 0;
}

int set_if_swaplimit(char *path, long unsigned mem_high, long unsigned mem_max)
{
	int init_length = strlen(path);
	char limit[STR_SIZE];
	if(mem_high)
	{
		strncat(path,"/memory.swap.high", STR_SIZE - 1);
		sprintf(limit, "%u", mem_high);
		if(mem_high == -1)
			strcpy(limit, "max");
		write_in(path, limit);
		path[init_length] = '\0';	// sterge ce a scris suplimentare
	}
	if(mem_max)
	{
		strncat(path,"/memory.swap.max", STR_SIZE - 1);
		sprintf(limit, "%u", mem_max);
		if(mem_max == -1)
			strcpy(limit, "max");
		write_in(path, limit);
		path[init_length] = '\0';
	}
	return 0;
}

int set_if_cpulimit(char *path, long unsigned quota, long unsigned timeout)
{
	if(quota == 0 || timeout == 0)
		return 1;
	int init_length = strlen(path);
	strncat(path,"/cpu.max", STR_SIZE - 1);
	char limit[256];
	if(timeout < 0)
		timeout = TIMEOUT_DEFAULT;
	if(quota < 0)
		sprintf(limit, "max %u", timeout);
	else
		sprintf(limit, "%u %u", quota, timeout);
	write_in(path, limit);
	path[init_length] = '\0';
}

int set_cpu_weight(char *path, long unsigned weight)
{
	if(weight == 0)
		return 1;
	int init_length = strlen(path);
	strncat(path,"/cpu.weight", STR_SIZE - 1);
	char limit[256];
	sprintf(limit, "%u", weight);
	write_in(path, limit);
	path[init_length] = '\0';
}

int delete_cgroup(struct cg_limits *cg)
{
	if(rmdir(cg->path) == -1)
		return -1;
	return 0;
}

int cg2_add_proc(struct cg_limits *cg, pid_t pid)
{
	char tasks_file[STR_SIZE] = {0};
	strncpy(tasks_file,cg->path,STR_SIZE - 1);
	strncat(tasks_file,"/cgroup.procs",STR_SIZE - 1);
	char pid_str[STR_SIZE];
	snprintf(pid_str,STR_SIZE - 1,"%d",pid);
	append_in(tasks_file,pid_str);
}

/* Make the folder in IZO_CG path and set the limits */
int create_cgroup2(struct cg_limits *the_limits)
{
	if(!the_limits)
		return -1;
	char *path = the_limits->path;
	if(mkdir(path, 0755) == -1){
		fprintf(stderr, "Eroare mkdir %s\n", path);
		errExit("mkdir");
	}
	set_cpu_weight(path, the_limits->cpu_weight);
	set_if_ramlimit(path, the_limits->mem_high, the_limits->mem_max);
	set_if_swaplimit(path, the_limits->mem_swap_high, the_limits->mem_swap_max);
	set_if_cpulimit(path, the_limits->cpu_quota, the_limits->cpu_timeout);

	return 0;
}

static int cg_read_cpu_max(struct cg_limits *cg)
{
	char path[PATH_MAX];
	sprintf(path, "%s/%s", cg->path, "cpu.max");
	FILE *f = fopen(path, "r");
	if(!f)
		return -1;
	char buf[STR_SIZE];
	if(fgets(buf, STR_SIZE, f) == NULL){
		fclose(f);
		return -1;
	}
	fclose(f);

	char *p = strtok(buf, " ");
	if(strcmp(p, "max") == 0)
		cg->cpu_quota = -1;
	else 
		cg->cpu_quota = atoi(p);
	p = strtok(NULL," ");
	cg->cpu_timeout = atoi(p);

	return 0;
}

static int cg_read_one_value(struct cg_limits *cg, char *file_name,
		long unsigned *cg_field)
{
	char path[PATH_MAX];
	sprintf(path, "%s/%s", cg->path,file_name);
	FILE *f = fopen(path, "r");
	if(!f)
		return -1;
	char buf[STR_SIZE];
	if(fgets(buf, STR_SIZE, f) == NULL){
		fclose(f);
		return -1;
	}
	fclose(f);

	if(strstr(buf, "max"))
		*cg_field = -1;
	else
		*cg_field = atoi(buf);

	return 0;
}

/* Citeste si completeaza campurile unei structuri din
 * sistemul de fisiere
 * @pid - pid-ul procesului, il foloseste pentru a gasi cgroup-ul
 * @cg - o structura deja initializata*/
static struct cg_limits *cg_read(pid_t pid, struct cg_limits *cg)
{
	if(!cg_read_path(pid, cg))
		errExit("cg_read_path");
	if(cg_read_cpu_max(cg) == -1)
		perror("Cpu max");
	if(cg_read_one_value(cg, "cpu.weight", &(cg->cpu_weight)) == -1)
		perror("weight");
	cg_read_one_value(cg, "memory.high", &(cg->mem_high));
	cg_read_one_value(cg, "memory.max", &(cg->mem_max));
	cg_read_one_value(cg, "memory.swap.high", &(cg->mem_swap_high));
	cg_read_one_value(cg, "memory.swap.max", &(cg->mem_swap_max));

	return cg;
}

static int cg_apply_limits(struct cg_limits *cg)
{
	set_if_ramlimit(cg->path, cg->mem_high, cg->mem_max);
	set_if_swaplimit(cg->path, cg->mem_swap_high, cg->mem_swap_max);
	set_if_cpulimit(cg->path,cg->cpu_quota, cg->cpu_timeout);
	set_cpu_weight(cg->path, cg->cpu_weight);
}

static int cg_freeze(struct cg_limits *cg)
{
	char path[PATH_MAX];
	sprintf(path, "%s/%s", cg->path, "cgroup.freeze");
	write_in(path, "1");
}

static int cg_unfreeze(struct cg_limits *cg)
{
	char path[PATH_MAX];
	sprintf(path, "%s/%s", cg->path, "cgroup.freeze");
	write_in(path, "0");
}

struct cg_limits *cgv2_init(struct cg_limits *cg)
{
	cg->read = cg_read;
	cg->create_cgroup = create_cgroup2;
	cg->delete_cgroup = delete_cgroup;
	cg->add_proc = cg2_add_proc;
	cg->apply_limits = cg_apply_limits;
	cg->freeze = cg_freeze;
	cg->unfreeze = cg_unfreeze;

	return cg;
}
