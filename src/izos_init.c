// initializeaza partea de cgroups
#include "common.h"
#include <sys/mount.h>
#include <sys/statfs.h>
#include <linux/magic.h>
#include "caps.h"

const char cg_controllers[][16] = {
	"cpu",
	"io",
	"memory",
	"pids"
};
const int n_controllers = 4;

int init_mkdir()
{
	if(mkdir(ROOT_PATH, 0755) == -1){
		if(errno != EEXIST)
			errExit("mkdir root");
	}
	return 0;
}

// scrie controllerele in cgroups.subtrees
int write_subtrees(const char* path)
{
	char file_path[256];
	snprintf(file_path, 255, "%s/cgroup.subtree_control", path);
	int i;
	for(i=0;i<n_controllers;i++){
		char contr[16];
		snprintf(contr, 15, "+%s", cg_controllers[i]);
		if(write_in(file_path, contr) == -1)
			return -1;
	}

	return 0;
}

/*
 * Remount the cgroup fs to add nsdelegate
 */
int remount_delegate()
{
	if(mount("string", CGROUP_PATH, "cgroup2", MS_REMOUNT, "nsdelegate") == -1)
		errExit("remount delegate");
	return 0;
}

int grand_delegation()
{
	char path[STR_SIZE];
	strncpy(path, IZO_CG, STR_SIZE - 1);
	if(chown(path, t_uid, t_gid) == -1)
		errExit("chown");
	// ar trebui sa facem si pt IZO_CG/cgroup.procs
	return 0;
}

int get_cg_type()
{
	struct statfs buf;
	if(statfs(CGROUP_PATH, &buf) == -1)
		perror("statfs");
	if(buf.f_type == CGROUP2_SUPER_MAGIC){
		// unified type
		return 0;
	}
	if(buf.f_type == TMPFS_MAGIC){
		char path[PATH_MAX];
		sprintf(path, "%s/unified", CGROUP_PATH);
		if(statfs(path, &buf) == -1)
			perror("statfs");
		if(buf.f_type == CGROUP2_SUPER_MAGIC){
			// Hybrid type
			return 1;
		}
		else {
			// legacy type
			return 2;
		}
	}
	return -1;
}

int main(int argc, char *argv[])
{
	if(!caps_check()){
		fprintf(stderr, "Lipsesc capabilitati necesare\n"
				"Foloseste \"sudo set_caps\" sau sudo %s ...\n", argv[0]);
		exit(1);
	}

	int cg_type = -1;
	if( (cg_type = get_cg_type()) ){
		if(cg_type == 1)
			printf("Cgroup type: hybride\n");
		if(cg_type == 2)
			printf("Cgroup type: legacy\n");
		printf("Inca nu exista suport pentru tipul acesta de cgroups\n");
		exit(1);
	}

	remount_delegate();
	fprintf(stderr, "Adaugare controllers...");
	if(write_subtrees(CGROUP_PATH) == -1){
		fprintf(stderr, "Nu am putut adauga controller-ele\n");
		exit(-1);
	}
	fprintf(stderr, "OK\n");

	fprintf(stderr, "Adaugare izos cgroup...");
	if(mkdir(IZO_CG, 0755) == -1){
		if(errno != EEXIST)
			errExit("mkdir IZO_CG");
	}
	fprintf(stderr, "OK\n");

	fprintf(stderr, "Adaugare controllers in izos cgroups...");
	if(write_subtrees(IZO_CG) == -1){
		fprintf(stderr, "Nu am putut adauga controller-ele\n");
		exit(-1);
	}
	fprintf(stderr, "OK\n");

	fprintf(stderr, "Creare /tmp/izos...");
	init_mkdir();
	fprintf(stderr, "OK\n");
	grand_delegation();
	return 0;
}
