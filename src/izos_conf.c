/* izos_conf.c
 * Arata configurarea pentru cgroups a unei izolari
 * Poate modifica configurarea, necesita un fisiere de config*/
#include "common.h"
#include "cgroups.h"

void show_usage(char *pname)
{
	fprintf(stderr, "Usage: %s opt name [-f conf_file]\n"
			"opt:\n"
			"show - show limits of a cgroups\n"
			"set - set the limits, requires a conf_file\n", pname);
	exit(1);
} 

long read_pid(const char *path)
{
	int fd = open(path, O_RDONLY);
	if(fd < 0)
		errExit("readpid open");
	char buff[STR_SIZE];
	if((read(fd, buff, STR_SIZE - 1)) < 0)
		errExit("read read");
	errno = 0;
	char *ptr;
	long pid = strtol(buff, &ptr, 10);
	if(errno)
		errExit("valoare invalida");
	return pid;
}

int show_conf(char *argv[])
{
	char *name = argv[0];
	char path[PATH_MAX];
	sprintf(path, "%s/%s/init.pid", ROOT_PATH, name);
	long pid = read_pid(path);

	struct cg_limits *cg = cg_init(name, NULL);
	cg->read(pid, cg);
	cg_print(cg);

	cg_free(&cg);

	return 0;
}

int set_conf(int argc, char *argv[])
{
	char opt;
	char *conf_file = NULL;
	char *name = argv[0];
	while((opt = getopt(argc, argv, "f:")) != -1) {
		switch(opt) {
		case 'f':
			{
				conf_file = strdup(optarg);
			} break;
		}
	}

	char path[PATH_MAX];
	sprintf(path, "%s/%s/init.pid", ROOT_PATH, name);
	long pid = read_pid(path);

	char cg_path[PATH_MAX];
	sprintf(cg_path, "%s/%s", IZO_CG, name);
	struct cg_limits *cg = cg_init(name, cg_path);
	cg->read(pid, cg);

	if(cg_read_conf(conf_file, cg) == NULL)
		perror("cg_conf - read conf");
	cg_print(cg);
	cg->apply_limits(cg);

	cg_free(&cg);
	free(conf_file);
	return 0;
}

int main(int argc, char *argv[])
{
	if(argc < 3)
		show_usage(argv[0]);

	if(strcmp(argv[1],"show") == 0)
		show_conf(argv + 2);

	if(strcmp(argv[1],"set") == 0)
		set_conf(argc-2, argv + 2);

	return 0;
}
