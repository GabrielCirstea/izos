#ifndef _COMMON_H_
#define _COMMON_H_

#define _GNU_SOURCE
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sched.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/capability.h>
#include <string.h>
#include <limits.h>

#define errExit(msg)    do { fprintf(stderr, "file: %s, line: %d\n", __FILE__, __LINE__);\
							perror(msg); exit(EXIT_FAILURE); \
                        } while (0)
#define STACK_SIZE 1024*1024
#define STR_SIZE 256

#define IZOS_DEBUG 1

// niste id-uri de utilizator si grup
static const int t_uid = 1000, t_gid = 1000;
static const char ROOT_PATH[] = "/tmp/izos";
static const char CGROUP_PATH[] = "/sys/fs/cgroup";
static const char IZO_CG [] = "/sys/fs/cgroup/izos";

static int append_in(char *path, char *text)
{	// echo >> path
	FILE* f = fopen(path, "a");
	if(!f)
	{
		perror("append_in - fopen");
		return -1;
	}
	fprintf(f,text);
	fprintf(f,"\n");
	fclose(f);
	return 0;
}

static int write_in(const char *path, const char *text)
{
	FILE* f = fopen(path, "w");
	if(!f)
	{
		fprintf(stderr, "File: %s\n", path);
		perror("write_in - fopen");
		return -1;
	}
	fprintf(f,text);
	fprintf(f,"\n");
	fclose(f);
	return 0;
}

static int wait_on_pipe(int p[2])
{
	char ch;
	if (read(p[0], &ch, 1) < 0) {
		fprintf(stderr, "Failure, read from pipe returned < 0\n");
		exit(EXIT_FAILURE);
	}

	return 0;
	
}
#endif	// ifndef
