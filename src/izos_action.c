/* izos_action.c
 * Acest program va "ingheta", "dezgheta" si opri o izolare*/
#include "common.h"
#include "cgroups.h"

void show_usage(char *pname)
{
	fprintf(stderr, "Usage: %s action [-n name] [-p pid]\n"
			"Actions:\n"
			"freeze - freez all processes in a izo\n"
			"unfreeze - unfreeze processes\n"
			"stop = stop the izo\n", pname);
	exit(1);
}

long read_pid(const char *path)
{
	int fd = open(path, O_RDONLY);
	if(fd < 0)
		return 0;
	char buff[STR_SIZE];
	if((read(fd, buff, STR_SIZE - 1)) < 0)
		return 0;
	close(fd);
	errno = 0;
	char *ptr;
	long pid = strtol(buff, &ptr, 10);
	if(errno)
		return 0;
	return pid;
}

int main(int argc, char *argv[])
{
	if(argc < 3)
		show_usage(argv[0]);
	char *action = argv[1];
	char *name = NULL;
	int pid = 0;
	struct cg_limits *cg;
	char opt;
	while((opt = getopt(argc, argv, "n:p:")) != -1) {
		switch(opt) {
		case 'n':
			{
				name = optarg;
				cg = cg_init(name, NULL);
			} break;
		case 'p':
			{
				pid = atoi(optarg);
				cg = cg_init(name, NULL);
				cg = cg->read(pid, cg);
				name = cg->name;
			} break;
		}
	}
	if(strcmp(action, "freeze") == 0){
		cg->freeze(cg);
		return 0;
	}
	if(strcmp(action, "unfreeze") == 0){
		cg->unfreeze(cg);
		return 0;
	}
	if(strcmp(action, "stop") == 0){
		printf("izo action: kill\n");
		/* inghetam pentru a opri fork() */
		cg->freeze(cg);
		char path[PATH_MAX];
		sprintf(path, "%s/%s/init.pid", ROOT_PATH, cg->name);
		int pid = read_pid(path);

		/* No /tmp/izo/$name dir
		 * then kill each pid in cgroup */
		if(!pid){
			sprintf(path, "%s/cgroup.procs", cg->path);
			FILE *f = fopen(path, "r");
			while(!feof(f)){
				fscanf(f, "%d", &pid);
				kill(pid, SIGKILL);
			}
			fclose(f);
		} else {
			if(kill(pid, SIGKILL) == -1)
				errExit("kill");
		}
	}
	return 0;
}
