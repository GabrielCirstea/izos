/* set_caps.c
 * Seteaza capabilitatii pe fisierele pentru izos */
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/capability.h>
#include <sys/prctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <cap-ng.h>
#include <wait.h>

#define BUF_LEN 0x10000

char buf[BUF_LEN];
const char *cap_name[CAP_LAST_CAP+1] = {
	"cap_dac_override",
	"cap_setgid",
	"cap_setuid",
	"cap_setpcap",
	"cap_sys_ptrace",
	"cap_sys_admin",
};
const int cap_name_n = 6;

void print_proc_caps()
{
	FILE *f = fopen("/proc/self/status", "r");

	while (fgets(buf, BUF_LEN, f) != NULL)
		if (!strncmp(buf, "Cap", 3))
			printf("%s", buf);
	fclose(f);
}

void set_caps(char **files)
{
	const int n = cap_name_n;
	cap_t cap = cap_init();
	cap_clear(cap);
	cap_value_t cap_list[n];

	int i;
	for (i=0; i < n; i++) {
		cap_from_name(cap_name[i], &cap_list[i]);
		printf("%-20s %d\t\t", cap_name[i], cap_list[i]);
		printf("\n");
	}
	cap_set_flag(cap, CAP_INHERITABLE,n, cap_list, CAP_SET);
	cap_set_flag(cap, CAP_PERMITTED,n, cap_list, CAP_SET);
	cap_set_flag(cap, CAP_EFFECTIVE,n, cap_list, CAP_SET);
	printf("flag set\n");

	for(i=0; files[i]; i++){
		if(cap_set_file(files[i], cap) == -1){
			perror("cap_set_file");
			exit(-1);
		}
	}

	cap_free(cap);
}

int check_caps()
{
	cap_value_t wanted_caps[] = {CAP_SETFCAP};
	int wanted_n = 1;
	cap_t caps = cap_get_proc();

	int i;
	for(i=0; i<wanted_n; i++){
		cap_flag_value_t status = 0;
		if(cap_get_flag(caps, wanted_caps[i], CAP_EFFECTIVE, &status)){
			perror("cap_get_flag");
		}
		if(status != CAP_SET){
			cap_t wanted = cap_init();
			cap_set_flag(wanted, CAP_EFFECTIVE, wanted_n, wanted_caps, CAP_SET);
			ssize_t len;
			char *cap_str = cap_to_text(wanted, &len);
			fprintf(stdout, "Capabilitatiile necesare: %s\n", cap_str);
			printf("nu avem cap nr %d\n", i);
			cap_free(wanted);
			cap_free(cap_str);
			return 0;
		}
	}
	return 1;

	cap_free(wanted_caps);
}

int main()
{
	if(!check_caps())
		return 1;
	char *filse[] = {"izos_create", "izos_init", "izos_action", "izos_exec",
			"izos_conf", NULL};
	set_caps(filse);

	return 0;
}
