/*
 * Aici o sa facem partea de cgroups
 */
#include "cgroups.h"
#include "cgroup2.h"
#include <sys/statfs.h>
#include <linux/magic.h>

#define TIMEOUT_DEFAULT 100000

static cgroup_layout_t get_cg_type()
{
	struct statfs buf;
	if(statfs(CGROUP_PATH, &buf) == -1)
		perror("statfs");
	if(buf.f_type == CGROUP2_SUPER_MAGIC){
		// unified type
		return CGROUP_LAYOUT_UNIFIED;
	}
	if(buf.f_type == TMPFS_MAGIC){
		char path[PATH_MAX];
		sprintf(path, "%s/unified", CGROUP_PATH);
		if(statfs(path, &buf) == -1)
			perror("statfs");
		if(buf.f_type == CGROUP2_SUPER_MAGIC){
			// Hybrid type
			return CGROUP_LAYOUT_HYBRID;
		}
		else {
			// legacy type
			return CGROUP_LAYOUT_LEGACY;
		}
	}
	return CGROUP_LAYOUT_UNKNOWN;
}

/* initializeaza o structura cu valorile implicite
 * @name - nnumele cgroup-ului
 * @path - path-ul, in mod normal este sub /sys/fs/cgroup/izo
 * functia verifica ce cgroup layaout este disponibil si completeaza
 * pointerii functiilor*/
struct cg_limits *cg_init(char *name, char *path)
{
	struct cg_limits *lim = malloc(sizeof(*lim));
	if(name){
		strcpy(lim->name, name);
		sprintf(lim->path, "%s/%s", IZO_CG, name);
	} else {
		memset(lim->name, 0, STR_SIZE);
		memset(lim->path, 0, PATH_MAX);
	}
	if(path)
		strcpy(lim->path, path);

	lim->cpu_quota		= -1;
	lim->cpu_timeout	= TIMEOUT_DEFAULT;
	lim->cpu_weight		= 100;
	lim->mem_high		= -1;
	lim->mem_max		= -1;
	lim->mem_swap_high	= -1;
	lim->mem_swap_max	= -1;

	cgroup_layout_t layout = get_cg_type();
	if(layout == CGROUP_LAYOUT_UNIFIED){
		cgv2_init(lim);
	} else {
		return NULL;
	}
	return lim;
}

int cg_free(struct cg_limits **cg)
{
	free(*cg);
	*cg = NULL;

	return 0;
}

static void value_or(long val, char *res, char *or)
{
	if(val == -1)
		strcpy(res,or);
	else
		sprintf(res, "%lu", val);
}

void cg_print(struct cg_limits *cg)
{
	printf("name: %s\n", cg->name);
	printf("path: %s\n", cg->path);
	printf("cpu quota: %d\n", cg->cpu_quota);
	printf("cpu timeout: %lu\n", cg->cpu_timeout);
	printf("cpu weight: %lu\n", cg->cpu_weight);
	char val[STR_SIZE];
	value_or(cg->mem_high, val, "max");
	printf("memory high: %s\n", val);
	value_or(cg->mem_max, val, "max");
	printf("memory max: %s\n", val);
	value_or(cg->mem_swap_high, val, "max");
	printf("memory swap high: %s\n", val);
	value_or(cg->mem_swap_high, val, "max");
	printf("memory swap max: %s\n", val);
}

long unsigned calc_mem_bytes(const char *t)
{
	char s[STR_SIZE];
	strncpy(s, t, STR_SIZE - 1);
	int last = strlen(s) - 1;
	int multiple = 1;
	switch(s[last])
	{
		case 'K':
		case 'k':
			{
				multiple = 1024;
				s[last] = '\0';
			}break;
		case 'M':
		case 'm':
			{
				multiple = 1024 * 1024;
				s[last] = '\0';
			}break;
		case 'G':
		case 'g':
			{
				multiple = 1024 * 1024 * 1024;
				s[last] = '\0';
			}break;
	}

	return atoi(s) * multiple;
}

/* Read the cgroup path from /proc/PID/cgroup and put it in @cg
 * @cg has to be initializated */
struct cg_limits *cg_read_path(pid_t pid, struct cg_limits *cg)
{
	char proc_path[PATH_MAX];
	sprintf(proc_path, "/proc/%d/cgroup", pid);
	printf("path: %s\n", proc_path);

	FILE *f = fopen(proc_path, "r");
	if(!f)
		return NULL;
	char buf[STR_SIZE];
	char *p;

	while (fgets(buf, STR_SIZE - 1, f) != NULL){
		p = strstr(buf,"/izos/");
		if(p){
			// un fel de trim_right()
			p = strtok(p, "\n");
			sprintf(cg->path, "%s%s", CGROUP_PATH, p);
			sprintf(cg->name, "%s", p + 6);
			break;
		}
	}
	fclose(f);

	return cg;
}


/* Parseaza informatia din t si o pune in cg_field */
static int cg_parse_to_field(char *t, long unsigned *cg_field)
{
	if(strstr(t, "max")){
		*cg_field = -1;
		return 0;
	}
	t = strtok(t, "\n");
	*cg_field = calc_mem_bytes(t);
	return 0;
}

/* Citeste un fisier de configurara
 * Preia doar limitele pt cgroups
 * @path - path-ul catre fisier
 * @cg - structura cg_limits deja initializata */
struct cg_limits *cg_read_conf(const char *path, struct cg_limits *cg)
{
	if(!path)
		return NULL;
	FILE *f = fopen(path, "r");
	if(!f)
		return NULL;
	char *buf = malloc(STR_SIZE);
	size_t n = STR_SIZE;
	while(getline(&buf, &n, f) > 0){
		if(strstr(buf, "cpu.quota")){
			// sari de "="
			char *p = strtok(buf, "=");
			p = strtok(NULL, "=");
			cg_parse_to_field(p, &(cg->cpu_quota));
		}
		if(strstr(buf, "cpu.timeout")){
			// sari de "="
			char *p = strtok(buf, "=");
			p = strtok(NULL, "=");
			cg_parse_to_field(p, &(cg->cpu_timeout));
		}
		if(strstr(buf, "cpu.weight")){
			// sari de "="
			char *p = strtok(buf, "=");
			p = strtok(NULL, "=");
			cg_parse_to_field(p, &(cg->cpu_weight));
		}
		if(strstr(buf, "memory.high")){
			// sari de "="
			char *p = strtok(buf, "=");
			p = strtok(NULL, "=");
			cg_parse_to_field(p, &(cg->mem_high));
		}
		if(strstr(buf, "memory.max")){
			// sari de "="
			char *p = strtok(buf, "=");
			p = strtok(NULL, "=");
			cg_parse_to_field(p, &(cg->mem_max));
		}
		if(strstr(buf, "memory.swap.high")){
			// sari de "="
			char *p = strtok(buf, "=");
			p = strtok(NULL, "=");
			cg_parse_to_field(p, &(cg->mem_swap_high));
		}
		if(strstr(buf, "memory.swap.max")){
			// sari de "="
			char *p = strtok(buf, "=");
			p = strtok(NULL, "=");
			cg_parse_to_field(p, &(cg->mem_swap_max));
		}
	}

	fclose(f);
	free(buf);
	return cg;
}

/* Add a proces in a cgroup, using file path */
int cg_add_raw_proc(const char *path, int pid)
{
	char tasks_file[STR_SIZE] = {0};
	strncpy(tasks_file,path,STR_SIZE - 1);
	strncat(tasks_file,"/cgroup.procs",STR_SIZE - 1);
	char pid_str[STR_SIZE];
	snprintf(pid_str,STR_SIZE - 1,"%d",pid);
	append_in(tasks_file,pid_str);
}
