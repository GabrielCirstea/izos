/* cgroup2.h
 * Header pentru functiile folosite pe cgroup v2 */
#ifndef _IZO_CGROUP2_H
#define _IZO_CGROUP2_H

#include "common.h"

/* Asociaza pointerii pentru functiile respective */
struct cg_limits *cgv2_init(struct cg_limits *cg);

/* read all the field of struct from files */
struct cg_limits *cg2_read(pid_t pid, struct cg_limits *cg);

int set_if_ramlimit(char *path, long unsigned mem_high, long unsigned mem_max);
int set_if_swaplimit(char *path, long unsigned mem_high, long unsigned mem_max);
int set_if_cpulimit(char *path, const long unsigned quota, const long unsigned timeout);
int set_cpu_weight(char *path, long unsigned weight);
// int create_cgroup2(struct cg_limits *cg);
// int cg2_add_proc(struct cg_limits *cg, pid_t pid);
// int delete_cgroup(struct cg_limits *cg);
// int cg_apply_limits(struct cg_limits *cg);
void cg_print(struct cg_limits *cg);
// int cg_freeze(struct cg_limits *cg);
// int cg_unfreeze(struct cg_limits *cg);

#endif	// ifndef
