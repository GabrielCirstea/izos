/*
 * O metoda de a afisare a izolarilor pornite*/
#include <dirent.h>
#include "common.h"

/*
 * Returneaza numele containerelor din ROOT_PATH
 * */
char **get_izos_names()
{
	DIR *d = opendir(ROOT_PATH);
	struct dirent *dir;
	int n_dirs = 16;
	char **dirs = malloc(n_dirs * sizeof(*dirs));
	int i = 0;
	if(d == NULL)
		errExit("open dir");
	while((dir = readdir(d)) != NULL){
		if(dir->d_type == DT_DIR){
			if(i == n_dirs - 1){
				n_dirs *= 2;
				dirs = realloc(dirs, n_dirs * sizeof(*dirs));
			}
			dirs[i++] = strdup(dir->d_name);
		}
	}
	dirs[i] = NULL;
	closedir(d);

	return dirs;
}

int free_dirs(char **dirs)
{
	int i = 0;
	while(dirs[i]){
		free(dirs[i]);
		i++;
	}
}

void print_dirs(char **dirs)
{
	int i = 0;
	while(dirs[i]){
		printf("%s\n", dirs[i]);
		i++;
	}
}

int main()
{
	char **dirs = get_izos_names();
	print_dirs(dirs);
	free_dirs(dirs);
}
