/* create_izo.c
 * Lanseaza un proces intr-o "izolare"
 * Acest proces va fi si intr-un user ns cu UID 0
 * Se vor aplica limite defaul pt cgroup, daca nu se specifica un fisier de
 * config*/
#include "cgroups.h"
#include "caps.h"
#include <dirent.h>
#include <sys/mount.h>
#include <sys/capability.h>
#include <signal.h>

struct child_args {
	char **argv;
	int    pipe_fd[2];
	int    pipe_fd2[2];
	int    pipe_init[2];
	volatile int    exec_pid;
	char  *cg_path;
	char  *izos_name;
};

struct izos_pkg {
	char *name;
	char *path;
	struct child_args c_args;
	struct cg_limits *cg;
};

struct izos_pkg *izos_init(char *name, char *path, struct cg_limits *cg)
{
	struct izos_pkg *pkg = malloc(sizeof(*pkg));
	pkg->name = name;
	pkg->path = path;
	if(!path && name){
		pkg->path = malloc(STR_SIZE);
		sprintf(pkg->path, "%s/%s", ROOT_PATH, name);
	}
	pkg->cg = cg;

	return pkg;
}

int izos_free(struct izos_pkg **pkg)
{
	struct izos_pkg *p = *pkg;
	free(p->name);
	free(p->path);
	cg_free(&(p->cg));
	*pkg = NULL;
}

void sig_term(int sig)
{
	printf("sigterm");
	exit(sig);
}


int init_mkdir()
{
	if(mkdir(ROOT_PATH, 0755) == -1){
		if(errno != EEXIST)
			errExit("mkdir root");
	}
	return 0;
}

static void update_map(char *mapping, char *map_file)
{
	int fd, j;
	size_t map_len;     /* Length of 'mapping' */

	/* Replace commas in mapping string with newlines */

	map_len = strlen(mapping);
	for (j = 0; j < map_len; j++)
		if (mapping[j] == ',')
			mapping[j] = '\n';

	fd = open(map_file, O_RDWR);
	if (fd == -1) {
		fprintf(stderr, "open %s: %s\n", map_file, strerror(errno));
		exit(EXIT_FAILURE);
	}

	if (write(fd, mapping, map_len) != map_len) {
		fprintf(stderr, "write %s: %s\n", map_file, strerror(errno));
		exit(EXIT_FAILURE);
	}

	close(fd);
}

static void write_deny(pid_t pid)
{
	char file[256];
	sprintf(file,"/proc/%d/setgroups", pid);
	int fd = open(file, O_RDWR);
	if (fd == -1) {
		fprintf(stderr, "open %s: %s\n", file, strerror(errno));
		exit(EXIT_FAILURE);
	}

	if (write(fd, "deny", 4) != 4) {
		fprintf(stderr, "write %s: %s\n", file, strerror(errno));
		exit(EXIT_FAILURE);
	}

	close(fd);
}

/*
 * verifica daca ezista deja numele */
int ckeck_izo_name(const char *name)
{
	if(access(name, F_OK) == 0)
		return 1;
	return 0;
}

int make_izo_dir(const char * name)
{
	char dir[256];
	sprintf(dir, "%s/%s", ROOT_PATH, name);
	if(mkdir(dir, 0755) == -1){
		if(errno == EEXIST)
			return 1;
		errExit("make IZO_ROOT/$name");
	}

	return 0;
}

/*
 * Pune un fisier in care se afla pid-ul*/
int put_files(pid_t pid, const char *name)
{
	char path[PATH_MAX];
	char pid_str[10];
	sprintf(pid_str, "%d", pid);
	sprintf(path, "%s/%s/init.pid", ROOT_PATH, name);
	write_in(path, pid_str);

	return 0;
}

int rmdir_rec(const char *path)
{
	// copii nu faceti asa
	char cmd[STR_SIZE];
	sprintf(cmd, "rm -rf %s/*", path);
	system(cmd);
	return 0;
}

int cleanup_folder(struct izos_pkg *container)
{
	rmdir_rec(container->path);
	if(rmdir(container->path) == -1)
		errExit("cleanup rmdir");
	return 0;
}

/* We use another fork() to rezev PID 1 in PID ns
 * Some processes may not like to have PID 1*/
int simple_init(char *argv[])
{
	if(getuid() == 0)
		return -1;
	if(!fork()){
		execvp(argv[0], argv);
		fprintf(stderr, "Programul \"%s\" nu a fost gasit\n", argv[0]);
		errExit("execvp");
	}
	signal(SIGTERM, sig_term);

	int status;
	wait(&status);
	return status;
}

/*
 *	Va pregatii punctele de mount, le face slave.
 *	Va schimba hostname-ul, doar de chestie
 *	Va modifica root-ul pentru cgroups
 *	Apoi va executa programul
 */
int child_func(void *args)
{
	struct child_args *arg = (struct child_args *) args;

	/* Renunta la privilegiile de root */
	if(getuid() == 0)
		caps_user(t_uid, t_gid);

	int flags = CLONE_NEWUSER | CLONE_NEWUTS | CLONE_NEWNS | CLONE_NEWPID;
	if(unshare(flags) == -1)
		errExit("child unshare()");

	/* Anunta parintele sa faca maparea [U/G]ID */
	close(arg->pipe_fd2[0]);
	if(write(arg->pipe_fd2[1], "1", 1) != 1)
		perror("child pipe write");

	close(arg->pipe_fd[1]);
	wait_on_pipe(arg->pipe_fd);
	close(arg->pipe_fd[0]);

	// set hostname
	if(sethostname(arg->izos_name, strlen(arg->izos_name)) == -1)
		errExit("sethostname");

	if(mount("tmp", "/tmp", "tmpfs", 0, NULL) == -1){
		errExit("mount tmpfs");
	}

	if(mount(arg->cg_path ,arg->cg_path, NULL, MS_BIND, NULL) == -1){
		errExit("mount bind");
	}

	if(mount(arg->cg_path ,CGROUP_PATH, "cgroup2", MS_MOVE, NULL) == -1){
		errExit("mount move cgroup2");
	}


	// necesar pt PID ns
	int pid = fork();
	if(!pid){

		/* Remontarea sistemului de fisiere /proc
		 * este necesar sa existe PID 1 */

		if(mount("proc", "/proc", "proc", 0, NULL) == -1){
			perror("mount proc");
		}

		/* close file descriptors that are not for this process */
		close(arg->pipe_fd2[1]);

		close(arg->pipe_init[1]);
		wait_on_pipe(arg->pipe_init);
		close(arg->pipe_init[0]);

		if(unshare(CLONE_NEWCGROUP) == -1)
			errExit("simple init - unshare cgroups");

		simple_init(arg->argv);
		exit(0);
	}
	/* close file descriptors that are not for this process */
	close(arg->pipe_init[0]);
	close(arg->pipe_init[1]);

	arg->exec_pid = pid;
	if(close(arg->pipe_fd2[1]) == -1)
		perror("close fd2");
#ifdef IZOS_DEBUG
	fprintf(stderr,"debug: Simple init pid: %d\n", pid);
#endif

	int status;
	waitpid(pid, &status, 0);
	return status;
}

/*
 * Pregateste lansarea procesului in namespace-uri
 * Aici se va face maparea pt UID si GID */
int launch_child(char *stack, int size, struct izos_pkg *container)
{
	// ar trebui sa le transit ca parametrii
	char uid_map[STR_SIZE];
	char gid_map[STR_SIZE];
	sprintf(uid_map, "%d %d 1,0 100000 1", t_uid,t_uid);
	sprintf(gid_map, "%d %d 1,0 100000 1", t_gid,t_gid);


    if (pipe(container->c_args.pipe_fd) == -1)
        errExit("make pipe");
    if (pipe(container->c_args.pipe_fd2) == -1)
        errExit("make pipe parent");
    if (pipe(container->c_args.pipe_init) == -1)
        errExit("make pipe init");

	char *stack_top = stack + size;
	pid_t child_pid = clone(child_func, stack_top, SIGCHLD | CLONE_VM,
			&(container->c_args));
	if(child_pid == -1)
		errExit("clone in launch");

	/* asteapta sa faca un user ns */
	close(container->c_args.pipe_fd2[1]);
	wait_on_pipe(container->c_args.pipe_fd2);

    close(container->c_args.pipe_fd[0]);

	char map_path[PATH_MAX];
	snprintf(map_path, PATH_MAX, "/proc/%ld/uid_map", (long) child_pid);
	update_map(uid_map, map_path);
	snprintf(map_path, PATH_MAX, "/proc/%ld/gid_map", (long) child_pid);
	write_deny(child_pid);
	update_map(gid_map, map_path);

    close(container->c_args.pipe_fd[1]);

	/* Ateptam sa primim pid-ul procesului init din PID ns
	 * Apoi il vom scrie in init.pid
	 * Si putem semnala procesul sa porneasca */
	wait_on_pipe(container->c_args.pipe_fd2);
    close(container->c_args.pipe_fd2[0]);
	if(container->c_args.exec_pid == -1){
		fprintf(stderr, "nu am primit pid bun de la simple init\n");
		exit(-1);
	}
	struct cg_limits *cg = container->cg;

	cg->add_proc(cg, container->c_args.exec_pid);
	put_files(container->c_args.exec_pid, container->name);

	/* Resume the simple_init */
	close(container->c_args.pipe_init[0]);
	close(container->c_args.pipe_init[1]);

	return child_pid;
}

int main(int argc, char *argv[])
{
	if(argc < 2){
		fprintf(stderr, "Usage: %s [-n name] [-f conf_file]"
			   "[-d] cmd [args]\n"
			   "-d for dettach\n", argv[0]);
		exit(1);
	}
	if(!caps_check()){
		fprintf(stderr, "Lipsesc capabilitati necesare\n"
				"Foloseste \"sudo set_caps\" sau sudo %s ...\n", argv[0]);
		exit(1);
	}
	char opt;
	char *conf_file = NULL;
	char *name = strdup(argv[1]);
	while((opt = getopt(argc, argv, "n:f:")) != -1) {
		switch(opt) {
		case 'n':
			{
				free(name);
				name = strdup(optarg);
			} break;
		case 'f':
			{
				conf_file = optarg;
			} break;
		}
	}

	struct cg_limits *cg = cg_init(name, NULL);
	struct izos_pkg *container = izos_init(name, NULL, cg);

	if(ckeck_izo_name(container->name)){
		fprintf(stderr, "Nume deja luat\n");
		izos_free(&container);
		exit(1);
	}

	// argumentele pentru copil
	struct child_args c_arg;
	c_arg.argv = argv + optind;
	c_arg.izos_name = container->name;
	c_arg.exec_pid = -1;

	container->c_args = c_arg;

	cg->create_cgroup(cg);
	container->c_args.cg_path = cg->path;
	if(conf_file)
		cg_read_conf(conf_file, cg);
	cg->apply_limits(cg);

	init_mkdir();
	make_izo_dir(container->name);

	char *stack = malloc(STACK_SIZE);
	pid_t copil = launch_child(stack, STACK_SIZE, container);
	if(waitpid(copil, NULL, 0) == -1){
		errExit("waitpid");
	}
	fprintf(stderr, "Se inchide \"containerul\"\n");

	if(cg->delete_cgroup(cg) != 0)
		fprintf(stderr, "Error on delete cg: %s\n", strerror(errno));
	cleanup_folder(container);
	izos_free(&container);
	// free(stack);
	return 0;
}
