/*
 * join_ns.c - pune procesul in toate ns-urile unui alt proces
 * scopul este de a porni un proces in interiorul unui se de namespaceuri
 * in care se afla deja alt proces
 *
 * Codul este luat din implementarea de nsenter()
 * https://github.com/karelzak/util-linux/blob/master/sys-utils/nsenter.c */
#define _GNU_SOURCE
#include <dirent.h>
#include <errno.h>
#include <getopt.h>
#include <sched.h>
#include <stdbool.h>
#include <unistd.h>
#include <assert.h>
#include <grp.h>
#include <sys/stat.h>
#include "common.h"

static struct namespace_file {
	int nstype;
	const char *name;
	int fd;
} namespace_files[] = {
	/* Careful the order is significant in this array.
	 *
	 * The user namespace comes either first or last: first if
	 * you're using it to increase your privilege and last if
	 * you're using it to decrease.  We enter the namespaces in
	 * two passes starting initially from offset 1 and then offset
	 * 0 if that fails.
	 */
	{ .nstype = CLONE_NEWUSER,  .name = "ns/user", .fd = -1 },
	{ .nstype = CLONE_NEWCGROUP,.name = "ns/cgroup", .fd = -1 },
	{ .nstype = CLONE_NEWIPC,   .name = "ns/ipc",  .fd = -1 },
	{ .nstype = CLONE_NEWUTS,   .name = "ns/uts",  .fd = -1 },
	{ .nstype = CLONE_NEWNET,   .name = "ns/net",  .fd = -1 },
	{ .nstype = CLONE_NEWPID,   .name = "ns/pid",  .fd = -1 },
	{ .nstype = CLONE_NEWNS,    .name = "ns/mnt",  .fd = -1 },
	{ .nstype = 0, .name = NULL, .fd = -1 }
};


static pid_t namespace_target_pid = 0;
static int root_fd = -1;
static int wd_fd = -1;

static void open_target_fd(int *fd, const char *type, const char *path)
{
	char pathbuf[PATH_MAX];

	if (!path && namespace_target_pid) {
		snprintf(pathbuf, sizeof(pathbuf), "/proc/%u/%s",
			 namespace_target_pid, type);
		path = pathbuf;
	}
	if (!path)
		errExit("neither filename nor target pid supplied for");

	if (*fd >= 0)
		close(*fd);

	*fd = open(path, O_RDONLY);
	if (*fd < 0)
		errExit("open in target_fd");
}

static void open_namespace_fd(int nstype, const char *path)
{
	struct namespace_file *nsfile;

	for (nsfile = namespace_files; nsfile->nstype; nsfile++) {
		if (nstype != nsfile->nstype)
			continue;

		open_target_fd(&nsfile->fd, nsfile->name, path);
		return;
	}
	/* This should never happen */
	assert(nsfile->nstype);
}

static int get_ns_ino(const char *path, ino_t *ino)
{
	struct stat st;

	if (stat(path, &st) != 0)
		return -errno;
	*ino = st.st_ino;
	return 0;
}

static int is_same_namespace(pid_t a, pid_t b, const char *type)
{
	char path[PATH_MAX];
	ino_t a_ino = 0, b_ino = 0;

	snprintf(path, sizeof(path), "/proc/%u/%s", a, type);
	if (get_ns_ino(path, &a_ino) != 0)
		errExit("stat of 1 failed");

	snprintf(path, sizeof(path), "/proc/%u/%s", b, type);
	if (get_ns_ino(path, &b_ino) != 0)
		errExit("stat of 2 failed");

	return a_ino == b_ino;
}

int join_ns(pid_t target, int uid, int gid)
{

	namespace_target_pid = target;
	struct namespace_file *nsfile;
	int c, pass, namespaces = 0, setgroups_nerrs = 0, preserve_cred = 0;
	bool do_rd = false, do_wd = true;
	int do_fork = -1; /* unknown yet */

	for (nsfile = namespace_files; nsfile->nstype; nsfile++) {

		/* It is not permitted to use setns(2) to reenter the caller's
		 * current user namespace; see setns(2) man page for more details.
		 */
		if (nsfile->nstype & CLONE_NEWUSER
			&& is_same_namespace(getpid(), namespace_target_pid, nsfile->name))
			continue;

		namespaces |= nsfile->nstype;
	}

	for (nsfile = namespace_files; nsfile->nstype; nsfile++)
		if (nsfile->nstype & namespaces)
			open_namespace_fd(nsfile->nstype, NULL);
	if (do_rd)
		open_target_fd(&root_fd, "root", NULL);
	if (do_wd)
		open_target_fd(&wd_fd, "cwd", NULL);

	/*
	 * Update namespaces variable to contain all requested namespaces
	 */
	for (nsfile = namespace_files; nsfile->nstype; nsfile++) {
		if (nsfile->fd < 0)
			continue;
		namespaces |= nsfile->nstype;
	}

	if ((namespaces & CLONE_NEWUSER) && !preserve_cred) {
		/* We call setgroups() before and after we enter user namespace,
		 * let's complain only if both fail */
		if (setgroups(0, NULL) != 0)
			setgroups_nerrs++;
	}

	/*
	 * Now that we know which namespaces we want to enter, enter
	 * them.  Do this in two passes, not entering the user
	 * namespace on the first pass.  So if we're deprivileging the
	 * container we'll enter the user namespace last and if we're
	 * privileging it then we enter the user namespace first
	 * (because the initial setns will fail).
	 */
	for (pass = 0; pass < 2; pass ++) {
		for (nsfile = namespace_files + 1 - pass; nsfile->nstype; nsfile++) {
			if (nsfile->fd < 0)
				continue;
			if (nsfile->nstype == CLONE_NEWPID && do_fork == -1)
				do_fork = 1;
			if (setns(nsfile->fd, nsfile->nstype)) {
				if (pass != 0)
					errExit("reassociate to namespace '%s' failed");
				else
					continue;
			}

			close(nsfile->fd);
			nsfile->fd = -1;
		}
	}

	/* Remember the current working directory if I'm not changing it */
	if (root_fd >= 0 && wd_fd < 0) {
		wd_fd = open(".", O_RDONLY);
		if (wd_fd < 0)
			errExit("cannot open current working directory");
	}

	/* Change the root directory */
	if (root_fd >= 0) {
		if (fchdir(root_fd) < 0)
			errExit("change directory by root file descriptor failed");

		if (chroot(".") < 0)
			errExit(("chroot failed"));
		if (chdir("/"))
			errExit("cannot change directory to /");

		close(root_fd);
		root_fd = -1;
	}

	/* Change the working directory */
	if (wd_fd >= 0) {
		if (fchdir(wd_fd) < 0)
			errExit("change directory by working directory file descriptor failed");

		close(wd_fd);
		wd_fd = -1;
	}

	if (setgroups(0, NULL) != 0 && setgroups_nerrs)	/* drop supplementary groups */
		errExit("Fail setgroups");
	if (setgid(gid) < 0)
		errExit("Fail setgid");
	if (setuid(uid) < 0)
		errExit("Fail setuit");
	return 0;
}
