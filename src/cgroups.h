/* cgroups.h
 * Functii generale pentru cgroups
 * struct cg_limits este structura in care se tin detaliile cgroup-ului
 * Pointerii la functii sunt pentru a oferii o interfata generala folosita
 * ambelor versiuni pentru cgroups
 * Cgroups v1 inca neimplementat*/

#ifndef _IZO_CGROUPS_H
#define _IZO_CGROUPS_H

#include "common.h"

struct cg_limits {
	char name[STR_SIZE];
	char path[PATH_MAX];
	long unsigned cpu_quota;
	long unsigned cpu_timeout;
	long unsigned cpu_weight;
	long unsigned mem_high;
	long unsigned mem_max;
	long unsigned mem_swap_high;
	long unsigned mem_swap_max;
	struct cg_limits* (*read)(pid_t pid, struct cg_limits *cg);
	int (*create_cgroup)(struct cg_limits *cg);
	int (*delete_cgroup)(struct cg_limits *cg);
	int (*add_proc)(struct cg_limits *cg, pid_t pid);
	int (*apply_limits)(struct cg_limits *cg);
	int (*freeze)(struct cg_limits*);
	int (*unfreeze)(struct cg_limits*);
};
typedef enum {
	CGROUP_LAYOUT_UNKNOWN = -1,
	CGROUP_LAYOUT_LEGACY  =  0,
	CGROUP_LAYOUT_HYBRID  =  1,
	CGROUP_LAYOUT_UNIFIED =  2,
} cgroup_layout_t;

struct cg_limits *cg_init(char *name, char *path);

/*read the path and the name from files */
struct cg_limits *cg_read_path(pid_t pid, struct cg_limits *cg);

int cg_free(struct cg_limits **cg);
long unsigned calc_mem_bytes(const char *t);
int cg_add_raw_proc(const char *path, int pid);
int delete_cgroup(struct cg_limits *cg);
struct cg_limits *cg_read_conf(const char *path, struct cg_limits *cg);
void cg_print(struct cg_limits *cg);

#endif	// ifndef
