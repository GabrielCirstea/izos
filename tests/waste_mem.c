// waste memory, to check mem limit
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>


int main(int argc, char **argv)
{
	if(argc < 2)
	{
		fprintf(stderr, "usage %s <mem>\n", argv[0]);
		fprintf(stderr, "mem can be like 1, 1K, 1M, 1G\n");
		exit(1);
	}
	int last = strlen(argv[1]) - 1;
	int multiple = 1;
	switch(argv[1][last])
	{
		case 'K':
		case 'k':
			{
				multiple = 1024;
				argv[1][last] = '\0';
			}break;
		case 'M':
		case 'm':
			{
				multiple = 1024 * 1024;
				argv[1][last] = '\0';
			}break;
		case 'G':
		case 'g':
			{
				multiple = 1024 * 1024 * 1024;
				argv[1][last] = '\0';
			}break;
	}

	int mem = atoi(argv[1]);
	char *p = malloc(mem * multiple);
	if(p == NULL)
	{
		perror("malloc");
		exit(1);
	}
	int i;
	for(i = 0; i < mem * multiple; ++i)
		p[i] = i%2 ? 1 : 0;		// sa para ce se intampla ceva

	free(p);

	return 0;

}
