/* test_cgroups.c
 * porneste o izolare si verifica daca se aplica bine limitele pentru cgroup */
#include "../cgroups.h"
#include "../common.h"

int exec_izo(char *argv[])
{
	pid_t pid = fork();
	if(!pid){
		execve(argv[0], argv, NULL);
		errExit("execve");
	}
	return pid;
}

int stop_izo(char *name)
{
	char *argv[] = {"../izos_action", "stop", "-n", name, NULL};
	pid_t pid;
	if(!pid){
		execve(argv[0], argv, NULL);
		errExit("execve");
	}
	waitpid(pid, NULL, 0);
	return 0;
}

long read_pid(const char *path)
{
	int fd = open(path, O_RDONLY);
	if(fd < 0)
		errExit("open");
	char buff[STR_SIZE];
	if((read(fd, buff, STR_SIZE - 1)) < 0)
		errExit("read");
	errno = 0;
	char *ptr;
	long pid = strtol(buff, &ptr, 10);
	if(errno)
		errExit("valoare invalida");
	return pid;
}

int main()
{
	char izo_name[] = "test_cg";
	char *conf_files[] = {
		"test1.conf",
		// "test2.conf",
		// "test3.conf",
		NULL
	};
	int i;
	for(i=0; conf_files[i]; i++){
		char *argv[] = {"../izos_create", "-n", izo_name, "-f",
			conf_files[i], "bash", NULL};

		pid_t pid_exec = exec_izo(argv);

		struct cg_limits *cg = cg_init("test_cg", NULL);
		cg_read_conf(conf_files[i], cg);
		printf("Limitele asteptate:\n");
		cg_print(cg);
		printf("\n");
		sleep(1);
		char path[PATH_MAX];
		sprintf(path, "%s/%s/init.pid", ROOT_PATH, cg->name);
		pid_t izo_pid = read_pid(path);
		struct cg_limits *cg_actual = cg_init(cg->name, cg->path);
		cg_actual->read(izo_pid, cg_actual);

		printf("Limitele actuale:\n");
		cg_print(cg_actual);
		printf("\n");

		stop_izo(izo_name);
		waitpid(pid_exec, NULL, 0);

		cg_free(&cg);
		cg_free(&cg_actual);
	}

	return 0;
}
