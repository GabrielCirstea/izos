#!/usr/bin/sh
# laseanza procese in 3 cgroups diferite pentru a arata timpii de executie
# cu limitele diferite pe CPU

set -e

[[ $EUID -ne 0 ]] && echo "Trebuie rulat ca root" && exit 1

for i in {1..3}; do
	echo Rulam testul $i
	time ../izos_create -n "test_cpu$i" -f "test$i.conf" make waste_mem
	rm waste_mem
done;
