# Izos

Programul la care am lucrat permite pornirea proceselor intr-un mediu controlat,
care (1)este izolat, prin intermediul namespace-urilor, in anumite aspecte de restul
sistemului de operare si (2) are resurse limitate, de grupurile de control.

## izos_init

Programul creaza directorul */tmp/izos* in care se vor face directoarele pentru
fiecare "mediu controlat" creat, si directorul */sys/fs/cgroup/izos*, grupul de
control sub care se vor crea celelalte grupuri.
Pentru grupul de control v2 acesta adauga dispozitivele de control:

* CPU
* memory
* io

Acestea sunt limitele pe care le va aplica (momentan doar CPU si memory);

Aceste dispozitive se adauga prin scrierea lor in fisierele *cgroup.subtree_controllers*
respectiv *izos/cgroup.subtree_controllers*

Suplimmentar programul schimba detinatorul directorului */sys/fs/cgroup/izos*
in scopul de a realiza "delegarea" grupurilor de control.(fara succes)

## izos_create

Usage: izos_create [-n name] [-f conf_file] cmd [args]

* -n pentru numele "mediului izolat"
* -f pentru fisierul de configurare, in prezent contine doar limitele pentru
grupul de control

### Structura

Parinte -> copil -> simple_init -> program

Parinte:
* Verifica daca numele este disponibil
* Pregateste directorul /tmp/izos
* Creaza grupul de control respectiv cu limitele specificate(daca este cazul)
* Face maparea [U/G]ID pentru procesul copil
* Adauga procesul "simple init" in grupul de control

Copil:
* Trece in namespace-uri diferite
* asteapta ca parintele sa faca maparea [U/G]ID
* shimba hostname-ul cu cel specificat
* remonteza anumite puncte de mount
	* /tmp
	* muta /sys/fs/cgroup/izos/$name in /sys/fs/cgroup folosind optiunea de
*move* pentru mount. Aceasta actiune este menita sa ascunda restul grupurilor de
control de pe sistem.

simple_init:
* remonteaza sistemul de fisiere /proc
	* este necesara existenta procesului cu PID 1 pentru a putea remonta /proc
* dupa ce este adaugat in grupul de control acesta isi schimba namespace-ul pentru
grupurile de control.
* executa programul cerut de utilizator.

### sincronizare

Sincronizarea intre cele 3 procese principale ale programului: parinte, copil,
simple_init este foarte importanta.
Pentru ca parintele sa poata face maparea [U/G]ID pentru copil, acesta trebuie
sa fie deja intr-un alt unser ns.
Copilul are nevoie de maparea respectiva pentru a putea aplica operatii privilegiate
in namespace.
Analog se aplica si pentru adaugarea in cgroup, procesul trebuie sa existe inainte
de a fi adaugat, deoarece adaugarea se face prin PID-ul procesului.

Sincronizarea este realizata prin intermediul a 3 pipe-uri.

Pipe-urile sunt un mecanism simplu ce permit transmiterea datelor intre procese.
Insa datele pot fi transmise intr-un singur sens.

Intre parinte si copil exista doua pipe-uri:

* unul prin care copilul semnaleaza parintele
* unul prin care parintele semnaleaza copilul

Al treilea pipe este intre parinte si simple_init.
Acest pipe este folosit pentru a semnala simple_init ca a fost adaugat in cgroup
si ca poate lansa procesul cerut.

### De ce atatea procese?

O metoda mult mai simpla ar fi fost

Parinte -(clone_new(ns))-> copil/simple_init -> program

In structura curenta, copilul isi schimba namespace-urile folosind *unshare*,
acelas lucru se putea realiza folosind flag-urile necesare in apelul de *cloene*.
Copilul fiind deja in namespace-urile cerute, parintele putand face direct maparea
[U/G]ID.

Motivul este necesitatea privilegiilor ridicate pentru grupurile de control.
Acum folosesc privilegii pe fisierele binare ale programelor, dar acestea se pot
rula folosind sudo, ce schima UID-ul procesului.
Copilul inainte de apelarea *unshare* verifica si daca este cazul is schimba
[U/G]ID-ul pentru a renunta la privilegiile ridicate.

## izos_conf

Usage: izos_conf show/set name [-f conf_file]

Programul se foloseste de fisierele din /tmp/izos pentru a gasii PID-ul procesului
init din grupul de control respectiv.

Dupa caz:

* citeste limitele si le afiseaza
* citeste fisierul de configurare specificat si aplica limitele citite din acest
fisier.

Un exemplu de fisier:

```
cpu.quota = 20010
cpu.timeout = 200000
cpu.weight = 101
memory.high = 3M
memory.max = 5M
memory.swap.high = 3M
memory.swap.max = 5M
```

## izos_action

Usage: izos_action action [-n name] [-p pid]
Actions: 

* freeze - freez all processes in a izo
* unfreeze - unfreeze processes
* stop - stop the izo

Folosind fisierul cgroups.freeze, acest program poate "ingheta" respectiv
"dezgheta" grupul respectiv.
 Optiunea "stop" trimite un semnal de oprire procesului de "init".
 Daca procesul init este oprit, toate procesele din PID ns-ul respectiv vor fi
 oprite.

 Acest program poate primi ca argument numele "mediului controlat" sau un pid din
 acel mediu.

 **OBS**

 Indiferent de informatia data de utilizator, rezultatul este acelas.

 Daca se folosete *stop* si flagul -p pentru PID, va fi oprit tot grupul de control
 nu doar procesul respectiv.

## izos_exec

Permite pornirea altor programe intr-un "mediu" deja creat.

Usage: izos_exec <izo_name> <cmd> [args ...]

### Structura

Parinte -> copil -> program

Parinte:

* identifica grupul de cintro si procesul "init".
* creaza copilul
* adauga copilul in grupul de control cerut

Copil:

* intra in namespace-urile pricesului "init"
	* foloseste *setns*
	* programul este o versiune modificata de la [nsenter](https://github.com/karelzak/util-linux/blob/master/sys-utils/nsenter.c)
