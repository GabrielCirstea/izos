# IZOS

A dead simple way to manage coumputer resourse.

## What is izos?

Izos is trying to be a simple manager for resources. It can isolate processes
using namespaces and cgroups.
That way the user cand limit the amount of resoursez that a process or a group
of processes cand access

## Current stage

Right now izos can start a procces izolate in a new set of namespaces: UTS, mount,
pid, cgroup and user namespace.
This allow processes tu have:

* their own hostname
* set of pids	- maybe not that good for some applications
* mount points
* change the look on **/sys/fs/cgroup**
* user - inside an "isolated enviroment" each process can have UID 0(root) and
another UID. Right now the second UID and GID are 1000.

More Documentation about how the program is working, and future direction will
be writen.

## CLI

### init

To use izos, firs you have to use **izos_init**.
This program wil simply create some folders, and remount the cgroup file system
with the "delegated" option

### create izos

To spawn a process in an isolate enviroment use **izos_create**.

```
izos_create [-n name] [-f conf_file] cmd
```

The configuration files are for cgroups, to set limits on CPU and memory.
Soon will come tho IO limit.

The -n is for the name if the izo, if not specified, the cmd will be use as name
as well.

### join izos

To spawn another process in the same place use **izos_exec**.

```
izos_exec name cmd
```

*name* is the name of izo, that was given at creation time.

### Configurations

An configuration file look something like this:

```
cpu.quota = 20010
cpu.timeout = 200000
cpu.weight = 101
memory.high = 3M
memory.max = 5M
memory.swap.high = 3M
memory.swap.max = 5M
```

These are the values that can be given at the time being.

For memory an mesure cand be use: K,M,G.

The configuration can be check and changed at runtime with **izos_conf**.

```
izos_conf action [-f conf_file] name
```

The acctions are:

* show - will show the current configuration
* set - require [-f conf_file] and will read and set a new configuration

### Actions

**izos_action** have 3 actions:

* freeze
* unfreeze
* stop

```
izo_action action name
```

## TO DO

* Implement IO controller
* check if cgroup v2 is available
	* mount cgroup v2
	* if not cgroup v2 implement function for cgroup v1
* make the configuration less static
	* the root paths for files are writen in cod, and cannot be changed after
compiletime
	* also UID and GID for user namespace mapping ar writen in code and are not
  checked. If another user except UID = 1000 and GID = 1000 what to use izos, have
  to modify the cod (just 2 variables)

## root required

Because I couldn't manage to use cgroups without root privileges, all programs
from above require root privileges.

Planning on usig some file capabilities to go around this if nothing else.

## Info

[cgroups v2](https://www.kernel.org/doc/html/latest/admin-guide/cgroup-v2.html?highlight=cgroups)
[namespace](https://man7.org/linux/man-pages/man7/namespaces.7.html)
[namespace lwn article](https://lwn.net/Articles/531114/)
