# Makefile pentru fisierele de aici
# $@ - numele tintei
# $< - prima dependinta
# $^ - toate dependintele

CC:=gcc
EXE=izos_create izos_init izos_list izos_exec izos_conf izos_action set_caps
CGROUPS=cgroups.o cgroup2.o
FLAGS:=-lcap -g

SRC := src
OBJ := obj

SOURCES := $(wildcard $(SRC)/*.c, $(SRC)/week*/*.c)
OBJECTS := $(patsubst $(SRC)/%.c, $(OBJ)/%.o, $(SOURCES))

all: $(EXE) $(DIRS)

izos_create: $(OBJ)/izos_create.o $(OBJ)/cgroups.o $(OBJ)/cgroup2.o $(OBJ)/caps.o
	$(CC) -I$(OBJ) $^ -o $@ $(FLAGS)

izos_exec: $(OBJ)/izos_exec.o $(OBJ)/join_ns.o $(OBJ)/cgroups.o $(OBJ)/cgroup2.o\
$(OBJ)/caps.o
	$(CC) $^ -o $@ $(FLAGS)

izos_conf: $(OBJ)/izos_conf.o $(OBJ)/cgroups.o $(OBJ)/cgroup2.o
	$(CC) $^ -o $@

izos_action: $(OBJ)/izos_action.o $(OBJ)/cgroups.o $(OBJ)/cgroup2.o
	$(CC) -I$(OBJ) $^ -o $@

izos_init: $(OBJ)/izos_init.o $(OBJ)/caps.o
	$(CC) -I$(OBJ) $^ -o $@ $(FLAGS)

izos_list: $(OBJ)/izos_list.o
	$(CC) -I$(OBJ) $< -o $@

set_caps: $(OBJ)/set_caps.o
	$(CC) -I$(OBJ) $< -o $@ $(FLAGS)

caps.o: $(SRC)/caps.c
	$(CC) -I$(SRC) $< -c -o  $@ $(FLAGS)

# dirs
$(OBJ)/%/:
	mkdir $@

# objects
$(OBJ)/%.o: $(SRC)/%.c
	$(CC) -I$(SRC) $< -c -o $@

.PHONY: clean

clean:
	rm -f $(OBJ)/*.o $(EXE)
